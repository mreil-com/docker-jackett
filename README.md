# docker-jackett

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-jackett.svg)](https://bitbucket.org/mreil-com/docker-jackett/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Jackett](https://github.com/Jackett/Jackett) image

Based on [mreil/ubuntu-base][ubuntu-base].


## Usage

    docker run -i -t \
    -p 9117:9117 \
    -v VOLUME:/home/jackett/.config/Jackett \
    mreil/jackett


### Runtime

  Jackett runs under the user `jackett`.
  All files reside in the `/home/jackett` directory.
  To persist configuration data between runs,
  mount the `/home/jackett/.config/Jackett` directory to an external volume.


### Environment variables

  * **API_KEY**: sets the API KEY which is required to access the REST API. 
     Will be generated randomly at first start when not set.


## Build

    ./gradlew build

### Update Radarr version

Check [latest release](https://github.com/Jackett/Jackett/releases/latest)
and update JACKETT_VERSION_ARG variable in Dockerfile.

### Update Gradle version

    ./gradlew wrapper --gradle-version [VERSION]


## Releases

  See [hub.docker.com](https://hub.docker.com/r/mreil/jackett/tags/).


## License

  See [LICENSE](LICENSE).


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base
