#!/usr/bin/env bash

set -eu

echo ===================
echo Starting Jackett ...
echo ===================

# these should be set via the environment
JACKETT_HOME=~
CONFIG_DIR=~/.config/Jackett
CONFIG_FILE=${CONFIG_DIR}/ServerConfig.json

# $1: JSON key
# $2: value
replace_line_in_json() {
  if [[ -n "${2}" ]]; then
    sed -i "${CONFIG_FILE}" \
      -e "s/\"${1}\": .*,/\"${1}\": \"${2}\",/"
  fi
}

mkdir -p "$CONFIG_DIR"
if [[ ! -f "${CONFIG_FILE}" ]]; then
  echo Copying initial config...
  cp -prnv /runtime/config/* "$CONFIG_DIR"
fi

API_KEY=${API_KEY:-}
if [[ -f ${CONFIG_FILE} ]]; then
  # Do nothing if config file exists
  echo Config file "${CONFIG_FILE}" exists.

  if [[ -n "${API_KEY}" ]]; then
    echo API_KEY env variable set, replacing API key in JSON config...
    replace_line_in_json "APIKey" "$API_KEY"
  fi
else
  # Copy template
  echo Creating config file "${CONFIG_FILE}"...
  cp "${CONFIG_DIR}"/ServerConfig.template.json "${CONFIG_FILE}"

  # Create api key
  if [[ -z "$API_KEY" ]]; then
    API_KEY=$(tr -dc 'a-z0-9' </dev/urandom | fold -w 32 | head -n 1)
    echo Created new API KEY: "${API_KEY}"
  else
    echo Using API_KEY env variable: "$API_KEY"
  fi

  # Create instance id
  INSTANCE_ID=$(tr -dc 'a-z0-9' </dev/urandom | fold -w 64 | head -n 1)
  echo Created new INSTANCE ID: "${INSTANCE_ID}"

  # Replace values in JSON
  replace_line_in_json "APIKey" "$API_KEY"
  replace_line_in_json "InstanceId" "$INSTANCE_ID"
fi

echo Current config:
cat "$CONFIG_FILE"

${JACKETT_HOME}/Jackett/jackett --ListenPublic --Logging --NoUpdates
