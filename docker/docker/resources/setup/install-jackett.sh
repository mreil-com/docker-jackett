#!/usr/bin/env bash

set -euxo pipefail

# This set via build arg
RUN_USER=${RUN_USER}
DIST=${DIST}

# Fail if no run-user set
if [[ -z "$RUN_USER" ]]; then
  echo No run user set
  exit 1
fi

# Create run-user
groupadd "$RUN_USER"
useradd -g "$RUN_USER" -m "$RUN_USER"

# Install dependencies
apt-get update -y
apt-get install -y gnupg ca-certificates curl libicu66

su - "$RUN_USER" <<EOF
#
echo Installing Jackett as user: \$USER
mkdir -p /home/"$RUN_USER"/.config/Jackett
curl -L ${DIST} | tar xz --no-same-owner -C /home/"$RUN_USER"
#
EOF
